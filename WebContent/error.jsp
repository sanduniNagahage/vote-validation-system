<!DOCTYPE html>
<html>
<head>
<link href="https://bootswatch.com/4/lumen/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="https://bootswatch.com/4/lumen/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="https://bootswatch.com/4/lumen/_variables.scss" rel="stylesheet" type="text/css"/>
<link href="https://bootswatch.com/4/lumen/_bootswatch.scss" rel="stylesheet" type="text/css"/>
<meta charset="ISO-8859-1">
<title>Error Page</title>
</head>
<body>
    <div class="jumbotron">
        <h1 class="display-3">Vote Validation Unsuccessful</h1>
        <p class="lead">    </p>
        
        <h1>Dear ${name},</h1>
		<h2>${message}</h2>
		<h2>This is Error page</h2>
        
        
        <hr class="my-4">
        <p class="lead">
          <a class="btn btn-primary btn-lg" href="index.jsp" role="button">Go Back</a>
        </p>
      </div>
</body>
</html>