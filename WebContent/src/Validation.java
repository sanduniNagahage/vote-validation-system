

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Validation
 */

@WebServlet("/Validation")
public class Validation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       Citizen c = new Citizen();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Validation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 c.setFirstName(request.getParameter("fname"));
		 
		 c.setLastName(request.getParameter("lname"));
		 c.setAge(Integer.parseInt(request.getParameter("age")));
		 c.setCitizenship(request.getParameter("item"));
		 c.setHaveManyCitizenships(Boolean.parseBoolean(request.getParameter("verification")));
		 
		// System.out.println(c.getFirstName()+c.getAge()+c.getLastName()+c.getCitizenship()+c.isHaveManyCitizenships());
		 
		Validator validator = new Validator(request, response,c);
	}

}
