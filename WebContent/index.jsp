<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>

<head>

    <link href="https://bootswatch.com/4/flatly/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://bootswatch.com/4/flatly/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="https://bootswatch.com/4/flatly/_variables.scss" rel="stylesheet" type="text/css" />
    <link href="https://bootswatch.com/4/flatly/_bootswatch.scss" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="style.css">

    <meta charset="ISO-8859-1">
    <title>Vote Validation App</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Vote Validation System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
            </ul>
            
        </div>
    </nav>

    <div class="jumbotron">
        <h1 class="display-3">Vote Validation Form</h1>
        <hr class="my-4">
        <p class="lead"></p>


        <div class="form">
            <form action="Validation" name="voteValidation" method="Post">
                <fieldset id="fieldsetcss">
                    <div class="form-group">
                        <label class="col-form-label" for="inputDefault">Enter your first name
                        </label>
                        <input type="text" class="form-control" placeholder="Enter the first name" name="fname" id="fname" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label" for="inputDefault">Enter your last name
                        </label>
                        <input type="text" class="form-control" placeholder="Enter the last name" name="lname" id="lname" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label" for="inputDefault">Enter your age
                        </label>
                        <input type="text" class="form-control" placeholder="enter the age" name="age" id="age" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleSelect1">Current citizenship:</label>
                        <select class="form-control" id="exampleSelect1" name="item">
                            <option value="SriLankan">SriLankan</option>
                            <option value="Other">Other</option>

                        </select>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="verification" value="true" >I have multiple citizenships
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>



                </fieldset>
            </form>

        </div>

    </div>




</body>

</html>