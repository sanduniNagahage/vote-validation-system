
public class Citizen {
	private String firstName;
	private String lastName;
	private int age;
	private String citizenship;
	private boolean haveManyCitizenships;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCitizenship() {
		return citizenship;
	}
	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}
	public boolean isHaveManyCitizenships() {
		return haveManyCitizenships;
	}
	public void setHaveManyCitizenships(boolean haveManyCitizenships) {
		this.haveManyCitizenships = haveManyCitizenships;
	}
	
	
}
