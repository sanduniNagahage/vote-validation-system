import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Validator {
	HttpServletRequest request;
	HttpServletResponse response;
	Citizen c;
	
	
	
	Validator(HttpServletRequest request,HttpServletResponse response, Citizen c) throws ServletException, IOException{
		this.request = request;
		this.response=response;
		this.c=c;
		
		getValidate();
	}
	
	private void getValidate() throws ServletException, IOException{
		
		System.out.println(c.getFirstName()+c.getAge()+c.getLastName()+c.getCitizenship()+c.isHaveManyCitizenships());
		
		if(c.getAge()<=18) {
		
			String msg = "You are under-aged";
			request.setAttribute("name", c.getFirstName()+" "+c.getLastName());
			request.setAttribute("message", msg);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			//System.out.println("Error age");
			
		}else if(!c.getCitizenship().equals("SriLankan")) {
			String msg = "You are not a Sri Lankan Citizen!";
			request.setAttribute("name", c.getFirstName()+" "+c.getLastName());
			request.setAttribute("message", msg);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			//System.out.println("Error citizenship");
			
		}else if(c.isHaveManyCitizenships()) {
			//System.out.println("Error multi c");
			String msg = "You have multi citizenships";
			request.setAttribute("name", c.getFirstName()+" "+c.getLastName());
			request.setAttribute("message", msg);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			
		}else {
			String msg = "You are a valid user for vote in Sri Lanka";
			request.setAttribute("name", c.getFirstName()+" "+c.getLastName());
			request.setAttribute("message", msg);
			request.getRequestDispatcher("Success.jsp").forward(request, response);
			//System.out.println("succefully registerd");
		}
	
		
}
}
